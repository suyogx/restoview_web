FROM python:3.8

ENV PATH="/scripts:${PATH}"

COPY ./requirements.txt /requirements.txt

#uwsgi dependecies
# RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers
RUN apt-get update && apt-get install -y --no-install-recommends \
        tzdata \
        python3-setuptools \
        python3-pip \
        python3-dev \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# install environment dependencies
RUN pip3 install --upgrade pip 
RUN pip3 install -r /requirements.txt
# RUN apk del .tmp

RUN mkdir /app
COPY ./restoview /app
WORKDIR /app
COPY ./scripts /scripts

RUN chmod +x /scripts/*


RUN mkdir -p /vol/web/static
RUN adduser user
RUN chown -R user:user /vol
RUN chmod -R 755 /vol/web
USER user

CMD ["entrypoint.sh"]