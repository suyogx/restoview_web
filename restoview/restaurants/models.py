from django.db import models
from .cuisine_list import cuisine_list
from users.models import User


rating_list = [
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    ]

class Restaurant(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    city = models.CharField(max_length=255)
    cuisine_type = models.CharField(max_length = 120, choices = cuisine_list, blank = False)
    average_rating = models.CharField(max_length=2, null=True, blank=True)
    created_by = models.ForeignKey(to=User, on_delete=models.CASCADE, default="1")
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Visit(models.Model):
    restaurant_id = models.ForeignKey(to=Restaurant, on_delete=models.CASCADE, default=1)
    date_visited = models.DateField(auto_now_add=False)
    expense = models.IntegerField(null=False, blank=False)
    notes = models.TextField(null=True, blank=True)
    rating = models.IntegerField(choices=rating_list, default=1)
    created_by = models.ForeignKey(to=User, on_delete=models.CASCADE, default="1")
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.restaurant_id)

    class Meta:
        ordering: ['-added_on']

    