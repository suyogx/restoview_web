from django.http import request
from .models import Restaurant, Visit
from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .serializers import RestaurantSerializer, VisitSerializer
from rest_framework import permissions
from .permissions import IsOwner
from django.db.models import Avg


class RestaurantListAPIView(ListCreateAPIView):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    
    #set logged in user to owner of the post
    def perform_create(self, serializer):
        return serializer.save(created_by = self.request.user)

    #display created by logged in user posts
    def get_queryset(self):
        return self.queryset.filter(created_by = self.request.user)

class RestaurantDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    lookup_field = 'id'

    #display restaurant detail
    def get_queryset(self):
        visited_rest = Visit.objects.filter(restaurant_id = self.kwargs['id'])
        #get average rating
        get_average_rating = visited_rest.aggregate(Avg('rating'))
        average_rating_value = int(get_average_rating['rating__avg'])

        #get average expense
        get_average_expense = visited_rest.aggregate(Avg('expense'))
        average_expense_value = get_average_expense['expense__avg']
        
        return self.queryset.all()


# display all the visits in a restaurant
class RestaurantVisitListAPIView(ListCreateAPIView):
    serializer_class = VisitSerializer
    queryset = Restaurant.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    #display created by logged in user posts
    def get_queryset(self):
        visit_list = Visit.objects.filter(restaurant_id = self.kwargs['id'])
        return visit_list


class VisitListView(ListCreateAPIView):
    serializer_class = VisitSerializer
    queryset = Visit.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    #set logged in user to owner of the post
    def perform_create(self, serializer):
        return serializer.save(created_by = self.request.user)

    #display created by logged in user posts
    def get_queryset(self):
        return self.queryset.filter(created_by = self.request.user)


class VisitDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = VisitSerializer
    queryset = Visit.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )
    lookup_field = 'id'

    #display created by logged in user posts
    def get_queryset(self):
        return self.queryset.filter(created_by = self.request.user)