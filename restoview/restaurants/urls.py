from django.urls import path
from .views import RestaurantListAPIView, RestaurantDetailAPIView, RestaurantVisitListAPIView, VisitDetailAPIView, VisitListView
urlpatterns = [
    path('', RestaurantListAPIView.as_view(), name='restaurant-list'),
    path('<int:id>', RestaurantDetailAPIView.as_view(), name='restaurant-detail'),
    path('<int:id>/visits/', RestaurantVisitListAPIView.as_view(), name='restaurant-visit-list'),
    path('visit/', VisitListView.as_view(), name='visit-list'),
    path('visit/<int:id>', VisitDetailAPIView.as_view(), name='visit-detail'),
]
