from .models import Restaurant, Visit
from rest_framework import serializers
from django.db.models import Avg

class RestaurantSerializer(serializers.ModelSerializer):
    average_rating_value = serializers.SerializerMethodField('get_average_rating')
    average_expense_value = serializers.SerializerMethodField('get_average_expense')
    class Meta:
        model = Restaurant
        fields = [
            'name',
            'city',
            'cuisine_type',
            'id',
            'average_rating_value',
            'average_expense_value',
        ]

    def get_average_rating(self, restaurant):
        visited_rest = Visit.objects.filter(restaurant_id = restaurant.id)
        #get average rating
        get_average_rating = visited_rest.aggregate(Avg('rating'))
        average_rating_value = get_average_rating['rating__avg']
        return average_rating_value
    
    def get_average_expense(self, restaurant):
        visited_rest = Visit.objects.filter(restaurant_id = restaurant.id)
        #get average expense
        get_average_expense = visited_rest.aggregate(Avg('expense'))
        average_expense_value = get_average_expense['expense__avg']
        return average_expense_value

class VisitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Visit
        fields = [
            'restaurant_id',
            'date_visited',
            'expense',
            'notes',
            'rating',
            'id',
        ]