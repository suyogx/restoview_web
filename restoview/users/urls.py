from django.urls import path
from .views import CreateNewPassword, LoginView, PasswordTokenCheck, RegisterView, ResetEmailPassword, VerifyEmail
from rest_framework_simplejwt.views import TokenRefreshView

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='register'),
    path('verify-email/', VerifyEmail.as_view(), name='verify-email'),
    path('request-reset-password/', ResetEmailPassword.as_view(), name='request-password-reset'),
    path('reset-password/<uidb64>/<token>/', PasswordTokenCheck.as_view(), name='confirm-reset-password'), 
    path('reset-password-complete', CreateNewPassword.as_view(), name='reset-password-complete'), 
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
