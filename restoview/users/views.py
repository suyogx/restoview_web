from users.serializers import CreateNewPasswordSerializer, RegisterSerializer, EmailVerifySerializer, LoginSerializer, ResetEmailPasswordSerializer
from django.shortcuts import render
from .models import User
from rest_framework import generics, serializers, status, views
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from .utils import Util
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
import jwt
from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .utils import Util
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_bytes, smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError #check for conventional data
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from .tasks import send_reset_email

class RegisterView(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user_data = serializer.data
        user = User.objects.get(username = user_data['username'])

        token = RefreshToken.for_user(user).access_token
        #get domain name of the app
        current_site = get_current_site(request).domain
        # link to direct to
        relativeLink = reverse('verify-email')
        absurl = 'http://'+current_site+relativeLink+"?token="+str(token)
        
        email_body = 'Greeting '+user.username+  '\n Click on the below link to verify your email id \n' + absurl
        data = {
            'domain': absurl,
            'email_to': user.email,
            'email_body':email_body,
            'email_subject':'Verify your email'}
        Util.send_email(data)
        return Response(user_data, status=status.HTTP_201_CREATED)

class VerifyEmail(views.APIView): #to display fields on swagger
    serializer_class = EmailVerifySerializer
    token_param_config = openapi.Parameter(
        'token', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING
    )
    @swagger_auto_schema(manual_parameters=[token_param_config])
    def get(self, request):
        token = request.GET.get('token') #get token from verify link
        try:
            payload = jwt.decode(token,settings.SECRET_KEY,algorithms=["HS256"])
            user = User.objects.get(id = payload['user_id']) #get user from the verified link
            if not user.is_verified: #check if already verified
                user.is_verified = True
                user.save()
            return Response({'email':'Activation Successful'}, status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError as identifier: #if expired
            return Response({'error':'Activation link expired'}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError as identifier: #check for tampered link
            return Response({'error':'Invalid Token'}, status=status.HTTP_400_BAD_REQUEST)

class LoginView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data = user)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ResetEmailPassword(generics.GenericAPIView):
    serializer_class = ResetEmailPasswordSerializer

    def post(self, request):
        serializer = self.serializer_class(data = request.data)
        email = request.data.get('email', '')
        if User.objects.filter(email = email).exists():
            send_reset_email.delay(email)
            return Response({'success':'An email with a reset link has been sent to your registered email id'}, status=status.HTTP_200_OK)
        return Response({'Error':'Email id does not exist on the server'}, status=status.HTTP_400_BAD_REQUEST)

class PasswordTokenCheck(generics.GenericAPIView):
    serializer_class = CreateNewPasswordSerializer
    def get(self, request, uidb64, token):
        try:
            id = smart_str(urlsafe_base64_decode(uidb64)) #decode if exists
            user = User.objects.get(id=id)
            if not PasswordResetTokenGenerator().check_token(user, token):
                return Response({'Error':'Invalid token. Please try again.'}, status=status.HTTP_400_BAD_REQUEST)
            return Response({'success':True, 'message': 'Valid Credentials', 'uidb64': uidb64, 'token':token}, status=status.HTTP_200_OK)

        except DjangoUnicodeDecodeError as identifier:
            return Response({'Error':'Invalid token. Please try again.'}, status=status.HTTP_400_BAD_REQUEST)
        

class CreateNewPassword(generics.GenericAPIView):
    serializer_class = CreateNewPasswordSerializer

    def patch(self, request): 
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception = True)
        return Response({'success':True, 'message':'Password reset success'}, status=status.HTTP_200_OK)
