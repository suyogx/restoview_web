from users.models import User
from celery import shared_task
from .utils import Util
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_bytes, smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError #check for conventional data
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.urls import reverse


@shared_task(bind = True)
def send_reset_email(request, email):
    user = User.objects.get(email=email)
    uidb64 = urlsafe_base64_encode(smart_bytes(user.id)) #hash the id
    token = PasswordResetTokenGenerator().make_token(user) #make the token invalid after usage
    #get domain name of the app
    current_site = 'localhost:8000'
    print("current",current_site)
    # link to direct to
    relativeLink = reverse(
        'confirm-reset-password',
            kwargs={'uidb64':uidb64, 'token':token}
            )
    absurl = 'http://'+current_site+relativeLink
    email_body = 'Hello '+user.username+  '\n Click on the below link to reset your password \n' + absurl
    data = {
        'domain': absurl,
        'email_to': user.email,
        'email_body':email_body,
        'email_subject':'Verify your email'}
    Util.send_email(data)