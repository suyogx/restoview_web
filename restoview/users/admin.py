from django.contrib import admin
from users.models import User
from restaurants.models import Restaurant, Visit

admin.site.register([User, Restaurant, Visit])