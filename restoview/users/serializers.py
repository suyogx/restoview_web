from rest_framework import serializers
from rest_framework.utils import field_mapping
from .models import User
from django.contrib import auth
from rest_framework.exceptions import AuthenticationFailed
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.encoding import force_bytes, smart_str, force_str, smart_bytes, DjangoUnicodeDecodeError 
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

class RegisterSerializer(serializers.ModelSerializer):
    password=serializers.CharField(
        max_length=64,
        min_length=8,
        write_only=True
        )
    
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password',
            'is_chef'
        ]
    
    def validate(self, attrs):
        email = attrs.get('email', '')
        username = attrs.get('username', '')

        if len(username)<5:
            raise serializers.ValidationError(
        'Username should be longer than 5 characters')
        return attrs

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

class EmailVerifySerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=999)
    class Meta:
        model = User
        fields = ['token']

class LoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=64, min_length=8, write_only = True)

    class Meta:
        model = User
        fields = ['username', 'password', 'tokens']
    def validate(self, attrs):
        username = attrs.get('username', '')
        password = attrs.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if not user.is_verified:
            raise AuthenticationFailed('Your email id is not verified.')
        if not user: #check if user exists
            raise AuthenticationFailed('Invalid credentials')

        return{
            'email':user.email,
            'username':user.username,
            'tokens': user.tokens
        }

class ResetEmailPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()

    class Meta:
        fields = [
            'email'
        ]

class CreateNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(min_length = 8, max_length= 64, write_only=True)
    token = serializers.CharField(min_length = 1, write_only=True)
    uidb64 = serializers.CharField(min_length = 1, write_only=True)

    class Meta:
        fields=[
            'password',
            'token',
            'uidb64'
        ]

    def validate(self, attrs):
        try:
            password = attrs.get('password')
            token = attrs.get('token')
            uidb64 = attrs.get('uidb64')
            id = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(id=id)

            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed('Invalid Link. Please try again.')
            user.set_password(password)
            user.save()
        except Exception as e:
            raise AuthenticationFailed('Invalid Link. Please try again.')
        return super().validate(attrs)