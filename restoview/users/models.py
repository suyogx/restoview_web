from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db import models
from rest_framework_simplejwt.tokens import RefreshToken

class User(AbstractUser): 
    username = models.CharField(max_length=255, unique=True)
    email = models.EmailField(null=False, blank=False, unique=True)
    is_verified = models.BooleanField(default=False)
    is_chef = models.BooleanField(default=False)
    

    REQUIRED_FIELDS = ['email', 'password']

    def __str__(self):
        return self.username

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh':str(refresh),
            'access':str(refresh.access_token)
        }