## Task assignment
Aim of this task is to create 'restaurant visit diary'. Our users like travel and they would like to write down their own reviews restaurants they have visited to know where to go next / to whom to recommend. It is therefore necessary to record the Restaurant and the Visit. At the Restaurant it is necessary to know the name, place, type (type of cuisine). The visit should records the date of the visit, the expense, a note (where the user can write down what he / she has given and other findings) and an evaluation (values ​​ranging from 1 to 5).
The system should use DB (we use Postgresql), but the default SQLite will suffice. The project should contain a README, which will include steps on how to run the project. The system should use Django and djangorestframework packages.


## Requirements

### project launch (docker):
- set Docker (or docker-compose) to run. I.e. create `Dockerfile` (and possibly` docker-compose.yml`) (django + ideally Postgresql)
- Create a shell script for easy execution.
- bonus: set django + docker for local startup and production startup (with env variables)

### django rest framework
- user registration and login via API (use [JWT] to log in (https://github.com/davesque/django-rest-framework-simplejwt))
- Ability to list, create, edit, delete restaurants.
- Possibility to create a Visit to a certain Restaurant (A visit to the Restaurant can be created only by the user who created the given Restaurant)
- Show all visits (date) and average rating and spending of all visits in the restaurant listing and dateil.
- bonus_1: Documentation for api endpoints should be available (we use [drf_yasg] (https://github.com/axnsan12/drf-yasg) to automatically generate documentation).
- bonus_2: sending an email when forgetting the password to the email (just write the email to the console, see [django docs] (https://docs.djangoproject.com/en/2.2/topics/email/#console-backend)) using queue ( eg [Celery] (https://docs.celeryproject.org/en/latest/django/first-steps-with-django.html))
- #############bonus_3 connection to 3rd party API (eg Zomato, which could list restaurants in a location)

## testing
- suggest a way to run tests.
- use buildin django unittest module and / or pousks
- bonus_1: use fixtures in testing

.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
###Completed Modules:
- set Docker (or docker-compose) to run. I.e. create `Dockerfile`
- Create a shell script for easy execution.
- bonus: set django + docker for local startup and production ready
- user registration and login via API using simpleJWT
- Ability to list, create, edit, delete restaurants.
- Possibility to create a Visit to a certain Restaurant
    - reastaurants can be created by anyone which can be used by other users to log a visit
- Show all visits (date) and average rating and spending of all visits in the restaurant listing and detail.
- bonus_1: Documentation for api endpoints should be available (we use [drf_yasg] (https://github.com/axnsan12/drf-yasg) to automatically generate documentation).
- bonus_2: sending an email when forgetting the password to the email using celery queue



.
.
.
.
.




welcome to Restoview - an api to track your culinary path
## Using Docker
1. Clone the repo

2 Docker Setup
    Install Docker and Docker compose and run the following from the project's 
    root folder containing the Dockerfile. 
    # docker-compose build
    # docker-compose up

4. The application can be accessed from localhost:8000

5. To star docker on production.(Additional changes required, such as ip address changes.)
    # docker-compose -f docker-compose-deploy.yml up --build


## Alternative Setup
1. Clone the repo

2. Create a virtualenv using pipenv and Install dependencies from requirements.txt
    # pipenv install -r requirements.txt

3. Install redis-server
 ##Setup redis as broker
    # sudo apt update
    # sudo apt install redis-server
    # sudo systemctl restart redis.service 
    Enable access to localhost by following the below steps
        sudo nano /etc/redis/redis.conf
    Inside the file, find the supervised directive. 
    This directive allows you to declare an init system to manage Redis as a service, 
    providing you with more control over its operation. 
    The supervised directive is set to no by default. 
    Under the assumptions that you are running Ubuntu, which uses the systemd init system, change this to systemd:
            /etc/redis/redis.conf
            . . .

            # If you run Redis from upstart or systemd, Redis can interact with your
            # supervision tree. Options:
            #   supervised no      - no supervision interaction
            #   supervised upstart - signal upstart by putting Redis into SIGSTOP mode
            #   supervised systemd - signal systemd by writing READY=1 to $NOTIFY_SOCKET
            #   supervised auto    - detect upstart or systemd method based on
            #                        UPSTART_JOB or NOTIFY_SOCKET environment variables
            # Note: these supervision methods only signal "process is ready."
            #       They do not enable continuous liveness pings back to your supervisor.
            supervised systemd

    Refer https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04 for more information


4. From project's root folder
    # python manage.py migrate
    # python mange.py runserver

5. Run this from the root on a different terminal to start a celery worker
    # celery -A restoview worker --loglevel=info

6. The application can be accessed from 127.0.0.0:8000

