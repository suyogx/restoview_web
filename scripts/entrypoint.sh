#!/bin/sh

set -e

python manage.py collectstatic --noinput

uwsgi --socket :8000 --master --enable-threads --module restoview.wsgi

# gunicorn restoview.wsgi:application --bind 0.0.0.0:8000
